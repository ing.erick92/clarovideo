import React from 'react';

import {Provider} from 'react-redux'
import generateStore from './redux/store'

//Components
import Home from './components/Home/Home';
import Footer from './components/Footer/Footer'
import DetailMovie from './components/DetailMovie/DetailMovie';

import {BrowserRouter as Router, Route, Link, Routes} from 'react-router-dom';
import {useParams} from 'react-router-dom';

function App() {

  

  const store = generateStore()

  return (
    <Provider store={store}>
       <header className="App-header">
        <img src="https://www.clarovideo.com/webclient/sk_core/images/clarovideo-logo-sitio.svg" className="App-logo" alt="logo" />
      </header>
       <Router>
      <Routes>
     
          <Route path="/" element={<Home />} />
          {/* 👇️ handle dynamic path */}
          <Route path="/detailMovie/:Id" element={<DetailMovie />} />
          {/* 👇️ only match this when no other routes match */}
          <Route
            path="*"
            element={
              <div>
                <h2>404 Page not found etc</h2>
              </div>
            }
          />
        </Routes>

      </Router>
      <Footer/>
    </Provider>
  );
}

export default App;
