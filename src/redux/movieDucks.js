import axios from 'axios'

// constantes
const dataInicial = {
    movies : [],
    movie : [],
    moviesByUser:[]
}

// types
const OBTENER_MOVIES_EXITO = 'OBTENER_MOVIES_EXITO'
const SET_MOVIE = 'SET_MOVIE'
const OBTENER_MOVIE_EXITO = 'OBTENER_MOVIE_EXITO'
const OBTENER_MOVIE_BYUSER = 'OBTENER_MOVIE_BYUSER'

// reducer
export default function pokeReducer(state = dataInicial, action){
    switch(action.type){
        case OBTENER_MOVIES_EXITO:
            return {...state, movies: action.payload}
        case OBTENER_MOVIE_EXITO:
            return {...state, movie: action.payload}
        case OBTENER_MOVIE_BYUSER:
                return {...state, moviesByUser: action.payload}
        case SET_MOVIE:
                return {...state, movie: action.payload}
        default:
            return state
    }
}

// acciones
export const getMovies = () => async (dispatch, getState) => {

    // console.log('getState', getState().pokemones.offset)
    try {
        const res = await axios.get(`https://mfwkweb-api.clarovideo.net/services/content/list?device_id=web&device_category=web&device_model=web&device_type=web&device_so=Edge&format=json&device_manufacturer=generic&authpn=webclient&authpt=tfg1h3j4k6fd7&api_version=v5.93&region=mexico&HKS=2qdkdn8p58fmb24o5m5pid9uq6&quantity=50&from=0&level_id=GPS&order_way=ASC&order_id=50&filter_id=34270`)
        console.log(res)
        dispatch({
            type: OBTENER_MOVIES_EXITO,
            payload: res.data.response.groups
        })
    } catch (error) {
        console.log(error)
    }
}

export const obtenerMovie = (idMovie) => async (dispatch) => {

    try {
        const res = await axios.get(`https://mfwkweb-api.clarovideo.net/services/content/data?device_id=web&device_category=web&device_model=web&device_type=web&device_so=Edge&format=json&device_manufacturer=generic&authpn=webclient&authpt=tfg1h3j4k6fd7&api_version=v5.93&region=mexico&HKS=2qdkdn8p58fmb24o5m5pid9uq6&group_id=${idMovie}`)
      
        dispatch({
            type: OBTENER_MOVIE_EXITO,
            payload: res.data.response.group.common
            
        })
    } catch (error) {
        console.log(error)
    }
}

export const claerObtenerMovie = (data) => async (dispatch) => {

        dispatch({
            type: OBTENER_MOVIE_EXITO,
            payload: data
            
        })

}

export const getMoviesByValue = (prefix) => async (dispatch) => {

    try {
        const res = await axios.get(`https://mfwkweb-api.clarovideo.net/services/search/predictive?device_id=web&device_category=web&device_model=web&device_type=web&device_so=Edge&format=json&device_manufacturer=generic&authpn=webclient&authpt=tfg1h3j4k6fd7&api_version=v5.93&region=mexico&HKS=2qdkdn8p58fmb24o5m5pid9uq6&value=${prefix}&filterlist=34429,34263,34450,34451,34469,35707,36018,41283,38045,38064,40156,40351,46177,41574,41756,40344,44596,43021&suggest=1&movies=4&series=4&live_channels=4&events=4&genres=1&talents=4&users=4&unavailables=0`)
      
        dispatch({
            type: OBTENER_MOVIE_BYUSER,
            payload: res.data.response ? res.data.response : []  
            
        })
    } catch (error) {
        console.log(error)
    }
}






