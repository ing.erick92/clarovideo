import React, {useEffect,useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { getMoviesByValue} from '../../redux/movieDucks'
import { Outlet, Link } from "react-router-dom";

const Search = () => {

    const dispatch = useDispatch()
    const movieByUser = useSelector(store => store.movies.moviesByUser)
    console.log(movieByUser)
    const [prefixUser, setprefixUser] = useState('')

 
    useEffect(() => {
       dispatch(getMoviesByValue(prefixUser))
        
    }, [prefixUser])

  return (
    <>
    <div className='SearchInp'>

        <input 
        type="text"
        name="searchUser" 
        onChange={(e)=>  setprefixUser(e.target.value) }  
        value={prefixUser} 
        placeholder="Buscar pelicula o serie"
        autoComplete="off"
        className='c-s-input__prompt'
        />
        <i name="search" as="i" aria-hidden="true" className="c-s-icon fa fa-search"></i>

         <div className= {movieByUser.suggest ? 'c-s-results c-s-results--visible' :'c-s-results' }>

            {
                movieByUser.suggest ?
                movieByUser.suggest.map(movieSug => (
                <>
                    <Link className='detailSearch' to={`detailMovie/${movieSug.common.id}`}>
                        <h5 className='titleSer' key={movieSug.common.title}>{movieSug.common.title}</h5>
                        <img key={movieSug.common.id} src={movieSug.common.image_small} alt={movieSug.common.title} title={movieSug.common.title}/>
                    </Link>
                </>
                )) : "No hay datos"
            }
                 <p className='CatClaro'>Películas Catálogo Claro video</p>
            {
               
                movieByUser.prediction ?
                movieByUser.prediction.movies.movie.map(moviePred => (
                <>
                    <Link className='detailSearch' to={`detailMovie/${moviePred.id}`}>
                        <h5 className='titleSer' key={moviePred.title}>{moviePred.title}</h5>
                        <img key={moviePred.id} src={moviePred.image_small} alt={moviePred.title} title={moviePred.title}/>
                    </Link>
                </>
                )) : "No hay datos"
            }
        </div>

    </div>
    </>
  )
}

export default Search