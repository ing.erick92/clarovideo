import React, {useEffect, useState} from 'react'

import {useParams} from 'react-router-dom';

import {useDispatch, useSelector} from 'react-redux'
import { obtenerMovie,claerObtenerMovie} from '../../redux/movieDucks'


const DetailMovie = () => {
    const params = useParams();
    const dispatch = useDispatch()
    const movie = useSelector(store => store.movies.movie)
    console.log("dataMovie",movie)
    useEffect(() => {
        dispatch(obtenerMovie(params.Id))
        window.scrollTo(0, 0)

        return () => {
            //Limpiar state DetailMovie
            dispatch(claerObtenerMovie([]))
        };
    }, [])

    return (
 
    <div className='Detail'>
        <div className='col-3'>
            {
               movie.image_large ? <img src={movie.image_large} alt={movie.image_large_alt}/> : "Cargando"
            }
            
        </div>
        <div className='col-3'>
           <h1>{movie.title}</h1>
           {
               movie.extendedcommon ? 
               <h4>{movie.title } </h4>
              :null
              
           }
           {
              movie.extendedcommon ?
              <> 
              <p>{movie.extendedcommon.media.publishyear} <spn>  {movie.extendedcommon.media.duration}</spn></p>
     
              </>
             :null 
           }
           
           <p>{movie.large_description}</p>
           {
              movie.extendedcommon ? 
              <h5>Géneros:  {
                movie.extendedcommon.genres.genre.map((cat,index)=>(
                    <span key={index}> {cat.desc+','}</span>
                ))}</h5>
             :null 
           }

        </div>
        <div className='col-3'>
           <img src="https://clarovideocdn0.clarovideo.net/pregeneracion/cms/apa/uat_531eed34tvfy7b73a818a234/cv_mensual_subscrition_mexico_wp0_landscape.png?1586901837"/>
           <div className="PBI-buttons-container">
               <button className="vcard vcard-blue-button btn btn-default btn-lg primary" type="button" style={{background: "rgb(76, 111, 167)"}}>
                   <span className="space">Suscríbete Mensual $115 <i></i></span>
                </button>
                <button className="vcard vcard-blue-button btn btn-default btn-lg primary" type="button" style={{background: "rgb(76, 111, 167)"}}>
                    <span className="space">Suscríbete Anual $1149 <i></i></span>
                    </button>
            </div>
        </div>
        
           <div className='talents'>
               <h3>Talentos</h3>
               <ul className='tal'>
                   
                    <li>
                        <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAABjZJREFUeAHtW4d6ozgQFmADduIS5/2fcJ047uB688tWjmhFFVJ032k2WbAZtZ/pUoL5YnFnnjojEHZu6RtyBDyAmoLgAfQAaiKg2dxLoAdQEwHN5l4CPYCaCGg29xLoAdREQLO5l0APoCYCms2dlsA4SVgUDTSXaLa50wCmScrS0cgsApq9Ow1gPCIA01RziWabOwsg1DcIQhYOBmwwHJpFQaN3ZwEsqm7xXmOtRpo6CWAQBCwh+ycIttBVchLAGIARiIKCKGJxnIiPTl2dBFClsomjzsQ5AIMwZHAgMiXkkV0k5wAsC1vgkVXA/jao7gE4GpdiolLtUmZLD5wCMCRnURXzwbnAQ7tETgGYptVpG8DjHtohBN0CsEHe65oaOwNgRCkbfutIpHh1fLaeOwNgG8lKHQpp3AGwRaCcNFD1/5UEDocxC1sUTh/8kS2MKsdxQgLLgueqmdd57Kq2fT6rt9p/jRawMEQsFjzyfQot8I//FO+J4/7kecRuT37+3ZP/ed9FJUfjMUPax+53GoeOOOKH7nGDy7/3mAV/zP/j3JyBc4LxycvY7XYjznbUGkCAN52/sWEctxupZ24E3eOXl956zbOMbddf32A27bi1CuMtfX1+sON+33QM5/n22w3bfK1ag4eFtZZAgcaOBj2fcjaZz3npXXz/X7rer1e2Jqk7n06dpx3onpGOSJWmb29sMNDft2hjg0LYPw3Cy19/kcp2sHvFYbUBFJ1NplOWjrvZpBtJworMApzR9XrhBYMw/DtMAcD3+432iiOubvP39077xofdju13WzF1rWtnFZZH3W427ESqMJlBpeH3mtP5fKKSfczbwpDDQaWKshYMfZ4duRPbbdZc9aJR8yVA2jbU/ynPm0+uhlNPD6TOscDVx5JLkfSo9uPlfGYnao9rSMVTFeHFXC4XznemaxtCv5/LP72Ch/HVM20zM4n3Sgv7/LNk2fEoPan+eCU1zkkyoM5VhOecrwWAx8Oev9g2NrZqDsVnzeW/2Kr2/s5jKni3yWxWyw0GFBNepzO2I+1HOFxG2FxCn/ttvZlAMI3wpE+VledlCMDHMNnxQCp3ZjMKdepy3YzUF+lMdsy4bVOFFrCVkMAd2VuYi5fJq7ye78/QhPXqk8xJtUR/N+h4YxRAzOlhe5bcQVRtTUYUlsB5ALhoECkzHXjgMzkCzkdgllF2OLAtORkbZBxALEKo0ohSr9fJVLmuiCoyAPiUZ/xUQqwob8GJ3EgtwaeSUIyzXa+5p1YOYuDL3p1I1RyR/sEmqSgndV+Rl6xzPvDUq+WSwTHIBJVFmGOTrAKIhaEIoCLEgbPFe+3e75BOas0WC6aSUN3sRDWvuu+sA1g8NCQmx8th4gNdEesh5JB/r+SQftJPT/wbO3ZWbKBYNGyYqgw2TGKuumtK50D59VipiuCDJCfpzyMgKR0J6SdBEzOuv1oFsOxoBo5toCChTQVPrt1Xww6sqrANFVOZiIZYdGKzCmCiOHXVadYVjWIyBzbJGoCwfXwPw/DqIqpLotxli6wB2EW1kI7VFRdUQNkwFWJca06kzIGIichXBN3YNoDnRrE2UdQH5TbiMzIVVaAtnvd5tQIgVKrJuRcsDBKHoqdI1XgaSOlZTBnIFMXaBqV8bi4IeLQ1TVZUWJU1qBaGdA5FTwFekQclqQ96hipME2or8U36VPFYAbDO/qHUjjx2Q5JWJTW8JE+5dJP9W1t20LgKl2Uf4m2i+sKBIxCbEgoO2H+BSqsyG/SDkMlGVmJcAstU6VF6+iLJow3tFuAJkGErscGPTXEVwVaWgavi7/qdcQlUqRJsHNSwj2rxgbw17COOm8iOCqZDZU+7gqVqZ1wC5ewDEgPJ6QM8sSBUb+B85OMmsVRsEPx9Xo1KYDH7QFCMYioWa4oQN+ZkU2EbUa3BH2sjhOrzZclzNyqBwvtCMiAhJsETC4PKYizsi4BUJkTw9nE1KoHYHIK6mrZDMhDcQdGmEjx8XPOnE3Lbtp97OxujGhghTFVcp2rT93em52BUhX8bPLwM03MwCmDf0uRifx5AzbfiAfQAaiKg2dxLoAdQEwHN5l4CPYCaCGg29xLoAdREQLO5l0APoCYCms29BGoC+A+/ZmMOVpSl/wAAAABJRU5ErkJggg=='/>
                       
                    </li>
               </ul>
           </div>

    </div> 
  )
}

export default DetailMovie