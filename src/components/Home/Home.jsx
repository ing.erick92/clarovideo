import React, { useEffect, useState } from 'react'

import {useDispatch, useSelector} from 'react-redux'
import { getMovies} from '../../redux/movieDucks'

import { Outlet, Link } from "react-router-dom";
import Search from '../Search/Search';

const Home = () => {
    const dispatch = useDispatch()
    const pokemones = useSelector(store => store.movies.movies)

    useEffect(() => {
        dispatch(getMovies())
  
    }, [])

   

    return (
       
        <div className='Container'>  
             <Search/>
            <div className='cont-movies'>
         
            {
                pokemones ?
            pokemones.map( (item, index) => (

                        <div className='backImg' key={index}>
                            <Link to={`detailMovie/${item.id}`}>
                                <img src={item.image_small} alt={item.title_original} title={item.title_original} />
                                <span className="play"><i className="fa fa-play-circle-o grid-channel-icon"></i></span>
                            </Link>
                        </div>
            )) : "No hay datos"
            }

            </div>

        </div>
    )
}

export default Home
